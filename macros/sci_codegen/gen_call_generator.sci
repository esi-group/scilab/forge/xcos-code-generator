// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_call_generator(blk_i, flag, in, out, blocks, variables, Info, layer, fd)
    // Generate a call to a block for a specific flag
    //
    // Parameters
    //  blk_i: block index
    //  flag: vector describing the inputs
    //  in: vector describing the inputs
    //  out: vector describing the outputs
    //  blocks: vector describing all the blocks (including inputs and outputs)
    //  variables: vector describing the links as variable names
    //  Info: compiled information
    //  layer: selected layer to generate the code for
    //  fd: the file descriptor
    
    cpr = Info(2);

    if cpr.sim.funtyp(blk_i) == 3 then
        // Scilab block
        fn = "gen_scifunc";
    elseif cpr.sim.funtyp(blk_i) > -1 then
        // not a specific block, call the gen_ prefixed function
        fn = "gen_" + cpr.sim.funs(blk_i);
    elseif cpr.sim.funtyp(blk_i) == -1 then
        // ifthenelse block
        // TODO: implement the ifthenelse block
        error("ifthenelse block is not supported yet : "+string(bk));
    elseif cpr.sim.funtyp(blk_i) == -2 then
        // eventselect blk
        // TODO: implement the eventselect block
        error("eventselect block is not supported yet : "+string(bk));
    else
        error("Unknown block type "+string(bk));
    end
    
    if ~isdef(fn) then
        error("Unsupported block " + cpr.sim.funs(blk_i) + " ; provide a generator for it");
    end

    execstr(fn + "(blk_i, variables, Info, " + string(flag) + ", fd)");
endfunction
