// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_inputscode(in, out, blocks, variables, Info, layer, fd)
    // Generate defaults inputs
    //
    // Parameters
    //  in: vector describing the inputs
    //  out: vector describing the outputs
    //  blocks: vector describing all the blocks (including inputs and outputs)
    //  variables: vector describing the links as variable names
    //  Info: compiled information
    //  layer: selected layer to generate the code for
    //  fd: the file descriptor

    cpr = Info(2);
    xcs = Info(6);

    inputs_fd = mopen(path+'/'+layer_name+'_inputs.sci', "wt");

    mputstr("function [t", inputs_fd)
    for i=1:size(in, '*') 
        var = variables(cpr.sim.outlnk(cpr.sim.outptr(in(i))));
        mputstr(", "+var, inputs_fd);
    end
    mputl("] = "+layer_name+"_inputs()", inputs_fd)
    mputl("    //EMX?: emx_func_file(''"+layer_name+"_inputs.c'')", inputs_fd);
    mputl("    ", inputs_fd)
    mputl("    t = rand();", inputs_fd);
    for i=1:size(in, '*')
        var = variables(cpr.sim.outlnk(cpr.sim.outptr(in(i))));

        lnk = cpr.sim.outlnk(cpr.sim.outptr(in(i)));
        value = xcs.outtb(lnk);
        sz_value = size(value);

        select typeof(value)
        case "constant" then
            // generate a random value with the max double range to kill premature
            // value range analysis
            m = sprintf("%1.25E", nearfloat("pred",%inf));
            n = sprintf("%1.25E", nearfloat("pred",%inf) / 2);
            cast = "";
        case "int8" then
            m = sprintf("%d", 2 ^ 8);
            n = sprintf("%d", 2 ^ 7);
            cast = "int8";
        case "int16" then
            m = sprintf("%d", 2 ^ 16);
            n = sprintf("%d", 2 ^ 15);
            cast = "int16";
        case "int32" then
            m = sprintf("%d", 2 ^ 32);
            n = sprintf("%d", 2 ^ 31);
            cast = "int16";
        case "uint8" then
            m = sprintf("%d", 2 ^ 8);
            n = sprintf("%d", 0);
            cast = "uint8";
        case "uint16" then
            m = sprintf("%d", 2 ^ 16);
            n = sprintf("%d", 0);
            cast = "uint16";
        case "uint32" then
            m = sprintf("%d", 2 ^ 32);
            n = sprintf("%d", 0);
            cast = "uint32";
        else
            error(typeof(value) + " datatype not managed ; please report a bug.");
        end

        mputl("    " + var + " = " + cast + "(rand(" + sprintf("%d, %d", sz_value(1), sz_value(2)) + "));", inputs_fd);
    end
    mputl("endfunction", inputs_fd);
    mputl("", inputs_fd);
    mclose(inputs_fd)

    mputstr("[t", fd)
    for i=1:size(in, '*') 
        var = variables(cpr.sim.outlnk(cpr.sim.outptr(in(i))));
        mputstr(", "+var, fd);
    end
    mputl("] = "+layer_name+"_inputs();", fd)

endfunction
