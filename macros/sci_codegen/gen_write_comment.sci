// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_write_comment(blk_i, Info, fd)
    // Write a block comment describing a block
    //
    // Parameters
    //  blk_i: block index
    //  Info: compiled information
    //  fd: the file descriptor

    cpr = Info(2);
    corinv = cpr.corinv;

    // add a comment
    comment = ""
    if cpr.sim.uids(blk_i) <> "" then
        comment = comment + cpr.sim.uids(blk_i);

        // look for block path
        d = scs_m;
        indexpath_str = [];
        namedpath_str = [];
        for blk=corinv(blk_i)
            indexpath_str = [indexpath_str ; """objs""" ; string(blk) ; """model""" ; """rpar"""]
            namedpath_str($+1) = d.props.title(1);
            d = d.objs(blk).model.rpar;
        end
        indexpath_str = indexpath_str(1:$-2);
        
        comment  = [comment ; "scs_m(list(" + strcat(indexpath_str, ',') + "))"];
        comment  = [comment ; strcat(namedpath_str, '/')];

        if type(cpr.sim.funs(blk_i)) <> 10 then
            fun = "scifunc";
        else
            fun = cpr.sim.funs(blk_i);
        end
        comment  = [comment ; fun];

        if cpr.sim.labels(blk_i) <> "" then
            user_comment = strsplit((cpr.sim.labels(blk_i)), ascii(10));
            comment = [comment ; user_comment];
        end
    end
    if comment <> "" then
        mputl("", fd);
        mputl("    // " + comment, fd);
    end

endfunction
