// Copyright 2016 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.

function str = gen_value_string(val)
    // Generate a string used to allocate val
    //
    // Parameters
    //  val: the value to generate as a string
    //  str: a string representation of val
    
    function str = cast(str, val)
        select type(val)
        case 8 then
            tf=""
            it=inttype(val)
            
            if int(it/10)==1 then
                tf="u"
                it=modulo(it,10)
            else
                tf=""
            end
            
            tf=tf+"int"+string(8*it);
            str = tf + "("+str+")"
        end
    endfunction
    
    // implements some generators to reduce the pressure on Scilab parsers
    if and(val == 0) then
        str = msprintf("zeros(%d, %d)", size(val, 'r'), size(val, 'c'));
        str = cast(str, val);
    elseif and(val == 1) then
        str = msprintf("ones(%d, %d)", size(val, 'r'), size(val, 'c'));
        str = cast(str, val);
    else
        str = sci2exp(val);
    end    
endfunction
