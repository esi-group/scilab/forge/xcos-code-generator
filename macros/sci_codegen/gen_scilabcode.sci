// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function [layer_name, variables, in, out, blocks] = gen_scilabcode(scs_m, path, Info, scs_m_selected_path)
    // Generate the Scilab code of the main sub-diagram
    //
    // Calling Sequence
    //  [variables, in, out, blocks] = gen_scilabcode(scs_m, path, Info, scs_m_selected_path)
    //
    // Parameters
    //  scs_m: the diagram to generate the code for
    //  path: directory path to generate the file in
    //  Info: compiled information, mainly scicos_cpr and scicos_sim
    //  scs_m_selected_path: selected sub-path to emit the code for as a list (as an index)
    //  layer_name: name of the subsystem
    //  variables: list of variable names
    //  in: input indexes
    //  out: output indexes
    //  blocks: blocks to generate the code for

    cpr = Info(2);

    in = []
    out = []
    blocks = []
    tows = ""

    // select the block for this layer
    selected_depth = size(scs_m_selected_path, '*');
    for i=1:length(cpr.sim.funs)
        layer_path = cpr.corinv(i);
       
        if (scs_m_selected_path == layer_path(1:selected_depth)) then
            blocks($+1) = i;
        end

        if (scs_m_selected_path == layer_path(1:$-1)) then
            select cpr.sim.funs(i)
            case "xcg_input_sim" then
                ipar = cpr.sim.ipar(cpr.sim.ipptr(i));
                in(ipar) = i;
            case "xcg_output_sim" then
                ipar = cpr.sim.ipar(cpr.sim.ipptr(i));
                out(ipar) = i;
            case "tows_c" then
                ipar = cpr.sim.ipar(cpr.sim.ipptr(i):cpr.sim.ipptr(i+1)-1);
                tows = tows + ", " + ascii(ipar(3:2+ipar(2))); // Gather the To_workspace blocks variables
            end
        end
    end

    // retrieve layer information and name
    layer = list()
    for i=scs_m_selected_path
        layer = lstcat(layer, list("objs", i, "model", "rpar"))
    end
    parent_block = layer;
    parent_block($) = null(); // model
    parent_block($) = null(); // block
    
    layer_name = scs_m(lstcat(parent_block, list("model", "label")));
    // remove non printable char
    layer_name = strsubst(layer_name, " ", "_");
    layer_name = part(layer_name, regexp(layer_name, "/[a-zA-Z0-9_]/"));
    
    // allocate the variable names (to reference by name)
    if winH <> [] then waitbar(0.50, gettext("Allocating variables"), winH); end
    variables = emptystr(length(cpr.state.outtb), 1);
    // initialize the inputs names
    if in <> [] then
        labels = "u" + string(1:size(in, '*'))';
        index_with_labels = find(cpr.sim.labels(in) <> "");
        with_labels = in(index_with_labels);
        
        if with_labels <> [] then
            labels(index_with_labels) = cpr.sim.labels(with_labels);
        end
        variables(cpr.sim.outlnk(cpr.sim.outptr(in))) = labels;
    end
    // initialize the outputs names
    if out <> [] then
        labels = "y" + string(1:size(out, '*'))';
        index_with_labels = find(cpr.sim.labels(out) <> "");
        with_labels = out(index_with_labels);
        
        if with_labels <> [] then
            labels(index_with_labels) = cpr.sim.labels(with_labels);
        end

        variables(cpr.sim.outlnk(cpr.sim.outptr(out))) = labels;
    end

    
    // make user-defined variable names unique by appending "_"
    [sorted_v, sorted_k] = gsort(variables,'g','i');
    duplicates_mask = [%f ; (sorted_v(1:$-1) <> "") & (sorted_v(2:$) == sorted_v(1:$-1))];
    duplicates = find(duplicates_mask);

    while duplicates <> []
        variables(sorted_k(duplicates)) = variables(sorted_k(duplicates)) + "_";
        
        [sorted_v, sorted_k] = gsort(variables,'g','i');
        duplicates_mask = [%f ; (sorted_v(1:$-1) <> "") & (sorted_v(2:$) == sorted_v(1:$-1))];
        duplicates = find(duplicates_mask);
    end
    
    // use a default name for unknown variable
    vv = find(variables == "")';
    if vv <> [] then
        variables(vv) = "tmp" + string(1:size(vv, '*'))';
    end

    // retrieve the state size information
    z = cpr.sim.zptr(blocks+1) - cpr.sim.zptr(blocks);
    n_z = sum(z > 0);

    // Scilab variable definition
    if out <> [] then
        outputs = strcat(variables(cpr.sim.outlnk(cpr.sim.outptr(out))), ', ')
        outputs_with_t = ", " + outputs;
    else
        outputs = "";
        outputs_with_t = "";
    end
    if in <> [] then
        inputs = strcat(variables(cpr.sim.outlnk(cpr.sim.outptr(in))), ', ');
        if outputs <> "" then
            pre_inputs = ", " + inputs;
        else
            pre_inputs = inputs;
        end
        
        inputs_with_t = ", " + inputs;
    else
        inputs = ""
        pre_inputs = ""
        inputs_with_t = "";
    end
    if n_z > 0 then
        dstate = ", " + strcat("z" + string(blocks(find(z))), ', ');
    else
        dstate = "";
    end

    // re-order blocks accordingly to the dependency graph
    [v, ka, kb] = intersect(cpr.sim.ordclk(:,1), blocks);
    if ka == [] then
        if winH <> [] then
            close(winH);
            messagebox(sprintf("%s: constant dataflow detected, unable to generate code", "gen_stepcode"), "Code generation failure", "error");
        end
        error(sprintf("%s: constant dataflow detected, unable to generate code", "gen_stepcode"));
    end
    ka = gsort(ka, 'g', 'i');
    blocks = cpr.sim.ordclk(ka, 1)';

    // default scenario for C code generation
    if winH <> [] then waitbar(0.55, gettext("Generating scenario asset"), winH); end
    fd = mopen(path+'/'+layer_name+'_scenario.sce', "wt");
    gen_scenariocode(in, out, blocks, variables, Info, layer, fd, tows);
    mclose(fd);

    // step / doit / layer simulation function
    if winH <> [] then waitbar(0.6, gettext("Generating simulation function"), winH); end
    fd = mopen(path+'/'+layer_name+'.sci', "wt");
    if isempty(outputs) & isempty(dstate) then
        tows = part(tows, 3:$); // Avoid initial comma
    end
    mputl("function [" + outputs + dstate + tows + "] = " + layer_name + "(t" + inputs_with_t + dstate +")", fd);
    mputl("    //EMX?: emx_func_file(''"+layer_name+".c'')", fd);
    mputl("    ", fd);
    gen_stepcode(in, out, blocks, variables, Info, layer, fd);
    mputl("endfunction", fd);
    mclose(fd);
    
    ok = %t
endfunction
