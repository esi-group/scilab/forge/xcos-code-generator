// Copyright 2016 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_stepcode(in, out, blocks, variables, Info, layer, fd)
    // Generate step code
    //
    // Parameters
    //  in: vector describing the inputs
    //  out: vector describing the outputs
    //  blocks: vector describing all the blocks (including inputs and outputs)
    //  variables: vector describing the links as variable names
    //  Info: compiled information
    //  layer: selected layer to generate the code for
    //  fd: the file descriptor

    cpr = Info(2);
    funtyp = cpr.sim.funtyp;
    clkptr = cpr.sim.clkptr;

    // update output
    flag = 1;
    mputl("    ", fd);
    mputl("    // update outputs ", fd);
        
    // foreach simulation function, emit the corresponding Scilab code
    if winH <> [] then waitbar(0.65, gettext("Generating simulation function"), winH); end
    for blk_i=blocks
        gen_call_generator(blk_i, flag, in, out, blocks, variables, Info, layer, fd)
    end
    
    // update state
    flag = 2;
    z = cpr.sim.zptr(blocks+1) - cpr.sim.zptr(blocks);
    n_z = sum(z <> 0);
    if n_z > 0 then
        mputl("    ", fd);
        mputl("    // update states ", fd);
        
        // foreach simulation function, emit the corresponding Scilab code
        if winH <> [] then waitbar(0.75, gettext("Generating simulation function"), winH); end
        for blk_i=blocks(z > 0)
            gen_call_generator(blk_i, flag, in, out, blocks, variables, Info, layer, fd)
        end
    end
endfunction
