// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_initcode(in, out, blocks, variables, Info, layer, fd)
    // Generate init code using the initial output values
    // 
    // Parameters
    //  in: vector describing the inputs
    //  out: vector describing the outputs
    //  blocks: vector describing all the blocks (including inputs and outputs)
    //  variables: vector describing the links as variable names
    //  Info: compiled information
    //  layer: selected layer to generate the code for
    //  fd: the file descriptor

    cpr = Info(2);
    xcs = Info(6);

    /////////////////////////////////////////
    // Inline the re-initialization values //
    /////////////////////////////////////////

    // retrieve then generate outputs information
    if size(out, '*') > 0 then
        mputl("", fd);
        mputl("// initial output values ", fd);
    end
    for i=1:size(out, '*')
        lnk = cpr.sim.outlnk(cpr.sim.outptr(out(i)));
        
        var = variables(lnk);
        value = gen_value_string(xcs.outtb(lnk));

        mputl("" + var + " = " + value + ";", fd);
    end
    if size(out, '*') > 0 then
        mputl("", fd);
    end

    // retrieve then generate inputs information
    mputl("", fd);
    mputl("// initial input values ", fd);
    mputl("t = 0.;", fd);
    for i=1:size(in, '*')
        lnk = cpr.sim.outlnk(cpr.sim.outptr(in(i)));
        
        var = variables(lnk);
        value = gen_value_string(xcs.outtb(lnk));

        mputl(var + " = " + value + ";", fd);
    end
    if size(in, '*') > 0 then
        mputl("", fd);
    end

    // retrieve then generate state information
    z = cpr.sim.zptr(blocks+1) - cpr.sim.zptr(blocks);
    block_with_states = blocks(z > 0);
    if block_with_states <> [] then
        mputl("", fd);
        mputl("// initial state values ", fd);
    end
    for i=block_with_states
        var = "z"+string(i);

        blk_z_i = cpr.sim.zptr(i):cpr.sim.zptr(i+1)-1;
        if typeof(cpr.sim.funs(i)) == "function" then
            value = gen_value_string(xcs.z(blk_z_i(3:$)));
        else
            value = gen_value_string(xcs.z(blk_z_i));
        end
        // Ignore scifunc_block_m's superfluous two first rpar values

        mputl(var + " = " + value + ";", fd);
    end
    if block_with_states <> [] then
        mputl("", fd);
    end

    // tag variables globally to discard any analysis
    mputl("// preserve the API by discarding const and range analysis", fd);
    mputl("//EMX?: emx_var_nopropagation(t, ''all'');", fd);
    for i=1:size(out, '*')
        var = variables(cpr.sim.outlnk(cpr.sim.outptr(out(i))));
        mputl("//EMX?: emx_var_nopropagation(" + var + ", ''all'');", fd);
    end
    for i=1:size(in, '*')
        var = variables(cpr.sim.outlnk(cpr.sim.outptr(in(i))));
        mputl("//EMX?: emx_var_nopropagation(" + var + ", ''all'');", fd);
    end
    for i=block_with_states
        var = "z"+string(i);
        mputl("//EMX?: emx_var_nopropagation(" + var + ", ''all'');", fd);
    end

    ///////////////////////////////////////
    // Call the initialization functions //
    ///////////////////////////////////////

    mputl("", fd);
    mputl("// initialize all the blocks ", fd);
    mputl("", fd);
    
    // init them all
    flag=4
    for blk_i=blocks
        gen_call_generator(blk_i, flag, in, out, blocks, variables, Info, layer, fd)
    end
endfunction
