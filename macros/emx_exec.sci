// This file is released under the 3-clause BSD license. See COPYING-BSD.


function emx_exec(filename, outdir)
    if (argn(2) < 2) then
        outdir = 'build';
    end

    olddir = pwd();
    chdir(outdir);
    
    unix_w(basename(filename));

    chdir(olddir);
endfunction
