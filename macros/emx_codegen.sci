// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017-2018 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function [rep,stat,err] = emx_codegen(in, args)

    [lhs, rhs] = argn();
    if 0 > rhs | rhs > 2 then
        error(sprintf(_("%s: Wrong number of input arguments: %d to %d expected.\n"), "emx_codegen", 0, 2));
    end

    if rhs > 0 & typeof(in) <> "string" then
        error(sprintf(_("%s: Argument #%d: File expected"), "emx_codegen", 1));
    end
    if rhs > 0 & fileinfo(in(:)) == [] then
        error(sprintf(_("%s: Argument #%d: Scilab script file expected"), "emx_codegen", 1));
    end

    if rhs > 1 then
        if typeof(args) <> "st" then
            error(sprintf(_("%s: Argument #%d: struct expected"), "emx_codegen", 2));
        end
    else
        args = struct();
    end

    // look for the emx-codegen tool
    cmd = "emx-codegen ";
    [rep,stat,err]=unix_g(cmd + "--help")
    if stat <> 0 then
        cmd = fullfile(getenv("EMX_CODEGEN_PATH", ""), "emx-codegen") + " ";
        [rep,stat,err]=unix_g(cmd + "--help")
    end

    if stat <> 0 then
        // local emx-codegen is not detected, use the remote implementation
        if isfield(args, "outdir")
            outdir = args.outdir;
        else
            disp("Generating files in " + fullfile(TMPDIR, 'build'));
            outdir = "build"
        end
        if isfield(args, "version")
            version = args.version;
        else
            disp("Using Emmtrix CodeGen latest version");
            version = "latest"
        end
        rep = []
        err = []
        stat = emx_codegen_remote(in, outdir, version);
    else
        [rep,stat,err] = emx_codegen_local(in, args)
    end
endfunction

