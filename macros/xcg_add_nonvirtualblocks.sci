// Copyright 2016 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function [scs_m, paths] = xcg_add_nonvirtualblocks(scs_m, paths)
    // Add XCG_IN_f and XCG_OUT_f blocks in scs_m bounds
    //
    // Calling Sequence
    //  [scs_m, paths] = xcg_add_nonvirtualblocks(scs_m)
    //  [scs_m, paths] = xcg_add_nonvirtualblocks(scs_m, path) // for internal recursive call 
    //
    // Parameters
    // scs_m: a Superblock diagram
    // paths: a list of all the scs_m subpath
    //
    // Description
    // Adding non virtual ports at superblock bounds allow to reconstruct the 
    // whole diagram hierarchy after compilation. The idea is to put one 
    // XCG_IN_f (resp. XCG_OUT_f) each IN_f (resp. OUT_f) block and connect 
    // them have a direct dataflow dependency and thus a presence on the 
    // scicos_cpr structure.
    // This pass also enforce "always active" behavior for constants.
    
    if ~exists("paths", 'l') then paths = list(list()); end
    
    current_path = paths($);
    for i=1:length(scs_m.objs)
        if typeof(scs_m.objs(i)) == "Block" then
            
            // add boundaries blocks on each superblock layer
            select scs_m.objs(i).gui
            case "IN_f" then
                blk = XCG_IN_f("define");
                lnk = scicos_link( ..
                        from=[i 1 0], ..
                        to=[(length(scs_m.objs)+1) 1 1]);
                
                old_lnk_index = scs_m.objs(i).graphics.pout
                if old_lnk_index == 0 then
                    error(sprintf("Unconnected %s block %s (%d) at scs_m(%s)\n", "IN_f", scs_m.objs(i).model.label, i, sci2exp(current_path)));
                end
                scs_m.objs(old_lnk_index).from = [(length(scs_m.objs)+1) 1 0]
                
                blk.graphics.orig = scs_m.objs(i).graphics.orig
                blk.graphics.sz = scs_m.objs(i).graphics.sz
                
                // escape labels to become valid variable names
                label = scs_m.objs(i).model.label;
                label = strsubst(label, " ", "_");
                [label_start, label_final] = regexp(label, "/^[a-zA-Z_][a-zA-Z0-9_]*$/");
                if label_start <> [] & label_final <> [] & label_start < label_final then
                    blk.model.label = part(label, label_start:label_final);
                end
                
                blk.model.uid = scs_m.objs(i).model.uid;
                blk.model.ipar = scs_m.objs(i).model.ipar;
                
                blk.graphics.pin = length(scs_m.objs)+2;
                blk.graphics.pout = old_lnk_index;
                
                scs_m.objs($+1) = blk;
                scs_m.objs($+1) = lnk;
                scs_m.objs(i).graphics.pout = length(scs_m.objs);
            case "OUT_f" then
                blk = XCG_OUT_f("define");
                lnk = scicos_link( ..
                        from=[(length(scs_m.objs)+1) 1 0], ..
                        to=[i 1 1]);
                
                old_lnk_index = scs_m.objs(i).graphics.pin
                if old_lnk_index == 0 then
                    error(sprintf("Unconnected %s block %s (%d) at scs_m(%s)\n", "OUT_f", scs_m.objs(i).model.label, i, sci2exp(current_path)));
                end
                scs_m.objs(old_lnk_index).to = [(length(scs_m.objs)+1) 1 1]
                
                blk.graphics.orig = scs_m.objs(i).graphics.orig
                blk.graphics.sz = scs_m.objs(i).graphics.sz
                
                // escape labels to become valid variable names
                label = scs_m.objs(i).model.label;
                label = strsubst(label, " ", "_");
                [label_start, label_final] = regexp(label, "/^[a-zA-Z_][a-zA-Z0-9_]*$/");
                if label_start <> [] & label_final <> [] & label_start < label_final then
                    blk.model.label = part(label, label_start:label_final);
                end
                
                blk.model.uid = scs_m.objs(i).model.uid;
                blk.model.ipar = scs_m.objs(i).model.ipar;
                
                blk.graphics.pin = old_lnk_index;
                blk.graphics.pout = length(scs_m.objs)+2;
                
                scs_m.objs($+1) = blk;
                scs_m.objs($+1) = lnk;
                scs_m.objs(i).graphics.pin = length(scs_m.objs);
            end
            
            // enforce "always active" behavior for const block
            // as XCG_IN_f and XCG_OUT_f depends on U, constant values were not propagated through (only initialized)
            model = scs_m.objs(i).model;
            if model.evtin == [] & model.blocktype == 'd' & model.dep_ut(2) == %f then
                model.blocktype = 'c';
                model.dep_ut(2) = %t;
                
                scs_m.objs(i).model = model;
            end
            
            // recursive call to handle inner diagrams
            if typeof(scs_m.objs(i).model.rpar) == "diagram" then
                paths($+1) = lstcat(current_path, list("objs", i, "model", "rpar"));
                
               [scs_m.objs(i).model.rpar, paths] = xcg_add_nonvirtualblocks(scs_m.objs(i).model.rpar, paths);
            end
        end
    end
endfunction
