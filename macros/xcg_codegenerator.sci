// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function  [ok, blk, cpr, layer_name, variables, in, out, blocks] = xcg_codegenerator(scs_m, path, scs_m_selected_path, winH)
    // Transforms a given Scicos discrete and continuous SuperBlock into a C defined Block
    //
    // Calling Sequence
    //  [ok] = xcg_codegenerator(scs_m)
    //  [ok] = xcg_codegenerator(scs_m, path)
    //  [ok] = xcg_codegenerator(scs_m, path, 1)
    //  [ok] = xcg_codegenerator(scs_m, path, "foo")
    //  [ok, blk] = xcg_codegenerator(scs_m, path, 1)
    //
    // Parameters
    //  scs_m: diagram to generate the code for
    //  path: path to generate the code into, set to current path as default
    //  scs_m_selected_path : the selected superblock to generate the code for
    //  blk: interface function of a block for SIL

    if exists("path","local")==0 then
        path = pwd();
    end
    if exists("scs_m_selected_path" , "l") == 0 then
        scs_m_selected_path = []
    end
    // convert the selected path to a corinv compatible index
    if typeof(scs_m_selected_path) == "string" then
        if size(scs_m_selected_path, '*') == 1 then
            // for Simulink-like '/' separated path
            scs_m_selected_path = strsplit(scs_m_selected_path, '/');
        end

        // lookup to convert string to indexes
        d = scs_m;
        found = %f;
        indexed_path = [];
        for p = scs_m_selected_path(:)'
            for i = 1:length(d.objs)
                if typeof(d.objs(i)) <> "Block" then
                    continue
                end
                
                if d.objs(i).model.label == p then
                    indexed_path($+1) = i;
                    found = %t;
                    break;
                else
                    // escape and recheck the escaped label
                    layer_name = d.objs(i).model.label;
                    layer_name = strsubst(layer_name, " ", "_");
                    layer_name = part(layer_name, regexp(layer_name, "/[a-zA-Z0-9_]/"));
                    
                    if layer_name == p then
                        indexed_path($+1) = i;
                        found = %t;
                        break;
                    end
                end
            end
            if found & typeof(d.objs(i).model.rpar) == "diagram" then
                d = d.objs(i).model.rpar;
            else
                error(msprintf("%s: Argument #%d: Unable to resolve %s.\n", "xcg_codegenerator", 3, p));
            end
        end
        scs_m_selected_path = indexed_path;
    end
    if exists("winH" , "l") == 0 then
        winH = [];
    end
    
    
    // Pre-process the schema to enforce some properties
    if winH <> [] then waitbar(0.1, gettext("Pre-processing"), winH); end
    [scs_m, paths] = xcg_add_nonvirtualblocks(scs_m);

    // compile the whole diagram and initialize values
    if winH <> [] then waitbar(0.2, gettext("Compiling model"), winH); end
    tf = scs_m.props.tf;
    scs_m.props.tf = 0;
    Info = scicos_simulate(scs_m, 'nw');
    cpr = Info(2);
    ok = Info(4);

    // Scilab code generation by default
    if ok then
        if winH <> [] then waitbar(0.3, gettext("Generating Scilab code"), winH); end
        [layer_name, variables, in, out, blocks] = gen_scilabcode(scs_m, path, Info, scs_m_selected_path)
    end

    // optional interface function generator
    if ok & argn(1) > 1  then
        if winH <> [] then waitbar(0.9, gettext("Generating interface function"), winH); end
        blk = gen_interfacefunction(scs_m, path, Info, scs_m_selected_path)
    end
endfunction
