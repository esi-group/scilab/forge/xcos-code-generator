// This file is released under the 3-clause BSD license. See COPYING-BSD.


function retval = emx_codegen_build_exec(filename, outdir, version)
	retval = emx_codegen(filename, outdir, version);
	if (retval <> 0)
		return
	end
	
	emx_build(outdir);
	emx_exec(filename, outdir);
endfunction
