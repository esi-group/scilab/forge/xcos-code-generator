// This file is released under the 3-clause BSD license. See COPYING-BSD.

function json = emx_codegen_getURLjson(url)
    global emx_debug
    
    oldPath = pwd();
    chdir(TMPDIR)

    if (emx_debug)
        disp(url);
    end

    [filename, content] = getURL(url);
    deletefile(filename);
    
    chdir(oldPath);
    
    if (emx_debug)
        disp(content);
    end
    
    json = emx_JSONParse(content);
    if (isstruct(json) & isfield(json, 'error')) then
        error(json.error)
    end
endfunction

function retval = emx_codegen_data(in_data, version)
    retval = -1;
    
    baseurl = 'http://codegen.service.emmtrix.com/' + version + '/';
    
    url = baseurl + 'register';
    json = emx_codegen_getURLjson(baseurl + 'register.json');
    
    sessionurl = baseurl + 'session/' + json.id + '/';

    for i = 1:size(in_data,1)
        url = sessionurl + 'addfile.json' + '?filename=' + urlencode(in_data(i,1)) + '&content=' + urlencode(in_data(i,2));
        res = emx_codegen_getURLjson(url);
    end

    url = sessionurl + 'execute.json';
    res = emx_codegen_getURLjson(url);

    write(%io(2), res.output);
    if (res.retval <> 0) then
        retval = res.retval;
        return;
    end

    for filename = res.filelist;
        if (length(filename))
            mprintf("Retrieving file %s\n", filename);

            url = sessionurl + 'getfile/' + filename;
            getURL(url);
        end
    end

    url = sessionurl + 'close.json';
    res = emx_codegen_getURLjson(url);

    retval = 0;
    return;
endfunction

function retval = emx_codegen_remote(in_files, outdir, version)
    retval = -1;
    
    if (argn(2) < 2) then
        outdir = 'build';
    end
    if (argn(2) < 3) then
        version = 'latest';
    end
    
    in_data = [];
    
    for in_file = in_files(:)'
        if (~isfile(in_file)) then
            error('File '+in_file+' not found');
            return;
        end
        
        in_file2 = basename(in_file) + fileext(in_file);
        in_data($+1,1:2) = [in_file2, strcat(mgetl(in_file), ascii(10))];
    end

    olddir = pwd();
    
    
    if ~isdir(outdir) then
        mkdir(outdir);
    end
    chdir(outdir);

    try
        retval = emx_codegen_data(in_data, version);
    catch
        disp(lasterror());
    end
    
    chdir(olddir);
endfunction
