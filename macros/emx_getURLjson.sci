// This file is released under the 3-clause BSD license. See COPYING-BSD.

function json = emx_getURLjson(url)
    oldPath = pwd();
    chdir(TMPDIR)

    [filename, content] = getURL(url);
    deletefile(filename);
    
    chdir(oldPath);
    
    json = emx_JSONParse(content);
    if (isstruct(json) & isfield(json, 'error')) then
        error(json.error)
    end
endfunction
