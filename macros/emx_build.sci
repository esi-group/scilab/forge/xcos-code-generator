// This file is released under the 3-clause BSD license. See COPYING-BSD.

function emx_build(outdir)
    if (argn(2) < 1) then
        outdir = 'build';
    end

    olddir = pwd();
    chdir(outdir);
    
    unix_w('make -f Makefile.gen');
    
    chdir(olddir);
endfunction
