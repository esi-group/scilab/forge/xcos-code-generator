// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017-2018 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function xcg_to_scilab(blk, scs_m)
    // Generate plain Scilab code
    // 
    // blk: a scicos_block to change
    // scs_m: the diagram to update (containing blk)
    
    // working directory for all tools
    tmp = pwd()+filesep()+"generated";
    if isdir(tmp) then removedir(tmp); end
    createdir(tmp);
    cwd = pwd();
    
    if ~isdef("blk", "local") then
        scs_m_selected_path = [];
        for i=1:length(scs_m.objs)
            if typeof(scs_m.objs(i)) == "Block" then
                if scs_m.objs(i).gui == "SUPER_f" then
                    scs_m_selected_path = i;
                    break;
                end
            end
        end
    else
        scs_m_selected_path = [];
        for i=1:length(scs_m.objs)
            if typeof(scs_m.objs(i)) == "Block" then
                if scs_m.objs(i).model.label == blk.model.label then
                    scs_m_selected_path = i;
                    break;
                end
            end
        end
    end

    if scs_m_selected_path == [] then
        error(sprintf("%s: Unable to detect the superblock to generate the code for.", "xcg_to_scilab"));
    end
    
    // emit Scilab code
    winH = waitbar(0.0, gettext("Code generation started"));
    [ok, blk, cpr, layer_name, variables, in, out, blocks] = xcg_codegenerator(scs_m, tmp, scs_m_selected_path, winH);
    close(winH);
    
    // reporting dialog
    if (length(tmp) > 80) then
        work_directory = "... " + part(tmp, ($-76):$);
    else
        work_directory = tmp;
    end
    head = "<a href=""file://"+strsubst(tmp, "\", "/")+""">"+work_directory+"</a><br/><br/>";
    function_name = "<a href=""file://"+strsubst(tmp, "\", "/")+"/"+layer_name+".sci"">"+layer_name+".sci</a><br/>";
    inputs = strcat(variables(cpr.sim.outlnk(cpr.sim.outptr(in))), " ");
    outputs = strcat(variables(cpr.sim.outlnk(cpr.sim.outptr(out))), " ");
    statistics = msprintf("%d inputs: %s<br/>%d outputs: %s<br/>%d blocks", size(in, '*'), inputs, size(out, '*'), outputs, size(blocks, '*'));
    messagebox(["<html><body>"+head+function_name+statistics+"</body></html>"], "Code generation completed", "info");
endfunction
