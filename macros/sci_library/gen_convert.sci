// Copyright 2016 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_convert(blk_i, variables, Info, flag, fd)
    lnk_in = cpr.sim.inplnk(cpr.sim.inpptr(blk_i))
    lnk_out = cpr.sim.outlnk(cpr.sim.outptr(blk_i))
    
    v_in = variables(lnk_in);
    v_out = variables(lnk_out);

    select flag
    case 1
    	gen_write_comment(blk_i, Info, fd);
    	
    	select type(cpr.state.outtb(lnk_out))
    	case 1
    		mputl("    " + v_out + " = double(" + v_in +");", fd);
		case 8
			select inttype(cpr.state.outtb(lnk_out))
			case 1
				mputl("    " + v_out + " = int8(" + v_in +");", fd);
			case 2
				mputl("    " + v_out + " = int16(" + v_in +");", fd);
			case 4
				mputl("    " + v_out + " = int32(" + v_in +");", fd);
			case 11
				mputl("    " + v_out + " = uint8(" + v_in +");", fd);
			case 12
				mputl("    " + v_out + " = uint16(" + v_in +");", fd);
			case 14
				mputl("    " + v_out + " = uint32(" + v_in +");", fd);
			else
				error("convert: conversion not handled yet");
			end
		else
			error("convert: conversion not handled yet");
   		end
   	end
endfunction

