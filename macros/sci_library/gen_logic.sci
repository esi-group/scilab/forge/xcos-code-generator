// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017-2018 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_logic(blk_i, variables, Info, flag, fd)
    n_in = cpr.sim.inpptr(blk_i+1) - cpr.sim.inpptr(blk_i);
    n_out = cpr.sim.outptr(blk_i+1) - cpr.sim.outptr(blk_i);

    v_in = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i):cpr.sim.inpptr(blk_i)-1+n_in));
    v_out = variables(cpr.sim.outlnk(cpr.sim.outptr(blk_i):cpr.sim.outptr(blk_i)-1+n_out));

    select flag
    case 1
        gen_write_comment(blk_i, Info, fd);

        opar = cpr.sim.opar(cpr.sim.opptr(blk_i));
        idx = ones(size(opar, 'r'), 1) * (size(v_in, '*'):-1:1);

        truth_table = dec2bin(0:(size(opar, 'r')-1))';

        // split the table, see bug #14944
        table = part(truth_table,1)
        for i=2:size(v_in, '*')
            table = [table part(truth_table,i)];
        end

        // set a false or true comparaison operator
        false_indexes = table == "0";
        table(false_indexes) = " == int8(0)";
        table(~false_indexes) = " <> int8(0)";

        // append the variable
        table = matrix(v_in(idx), size(idx, 'r'), size(idx, 'c')) + table;

        // construct the conditions set
        comparisons = "(" + strcat(table, ') & (', 'c') + ")";
        conditions = emptystr(size(table, 'r'), 1);
        conditions(1) = "if " + comparisons(1) + " then";
        conditions(2:$-1) = "elseif " + comparisons(2:$-1) + " then";
        conditions($) = "else // " + comparisons($);

        // write the actions foreach condition
        for i=1:size(conditions, '*')
            mputl("    " + conditions(i), fd);
            for j=1:size(v_out,'*')
                mputl("    " + "    " + sci2exp(opar(i,j), v_out(j)) + ";", fd);
            end
        end
        mputl("    end", fd);
    end
endfunction

