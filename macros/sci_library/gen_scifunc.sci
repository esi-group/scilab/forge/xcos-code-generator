// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_scifunc(blk_i, variables, Info, flag, fd)
    existing_scifunc = ls(path + '/' + "scifunc_inner*.sci");
    
    // retrieve the original model values ; cpr definition is transformed and
    // only contains a Scilab functions wrapper
    indexes = list("objs");
    corinv = cpr.corinv(blk_i);
    for p=corinv(1:$-1)
        indexes($+1) = p;
        indexes($+1) = "model";
        indexes($+1) = "rpar";
        indexes($+1) = "objs";
    end
    indexes($+1) = corinv($);
    indexes($+1) = "model";
    model = scs_m(indexes);
    
    // retrieve common variables
    n_inputs = cpr.sim.inpptr(blk_i+1) - cpr.sim.inpptr(blk_i);
    n_outputs = cpr.sim.outptr(blk_i+1) - cpr.sim.outptr(blk_i);
    n_state = cpr.sim.zptr(blk_i+1) - cpr.sim.zptr(blk_i);
    
    //
    // Emit the local function to a file first at initialization
    //
    if flag == 4 then
        
        // access `path` variable on purpose, local functions are not supported on 
        // the code generator for now.
        fname = "scifunc_inner" + string(size(existing_scifunc, '*') + 1);
        scifunc_fd = mopen(path + '/' + fname + ".sci", 'wt');
        existing_scifunc($+1) = path + '/' + fname + ".sci";
        
        // Scilab variable definition (callee side)
        if n_outputs > 0 then
            outputs = strcat("y" + string(1:n_outputs), ', ')
        else
            outputs = "";
        end
        if n_inputs > 0 then
            inputs = ", " + strcat("u" + string(1:n_inputs), ', ');
        else
            inputs = ""
        end
        if n_state > 0 then
            dstate = ", z";
        else
            dstate = "";
        end
        
        mputl("function [" + outputs + dstate + "] = " + fname + "(flag" + inputs + dstate +")", scifunc_fd);
        mputl("    // " + model.uid, scifunc_fd);
        mputl("", scifunc_fd);
        mputl("    y" + string(1:n_outputs)' + " = [];", scifunc_fd);
        mputl("", scifunc_fd);
        mputl("    if flag == 1 then", scifunc_fd);
        mputl("        // output", scifunc_fd);
        mputl("        " + model.opar(1), scifunc_fd);
        if n_state > 0 then
            mputl("    elseif flag == 2 then", scifunc_fd);
            mputl("        // state", scifunc_fd);
            mputl("        " + model.opar(3), scifunc_fd);
        end
        mputl("    elseif flag == 4 then", scifunc_fd);
        mputl("        // initialization", scifunc_fd);
        mputl("        " + model.opar(5), scifunc_fd);
        mputl("    end", scifunc_fd);
        mputl("endfunction", scifunc_fd);
        mclose(scifunc_fd);
    end

    //
    // Emit the caller code
    //

    // look for the generated function name
    fname = [];
    header = "    // " + model.uid;
    for f=existing_scifunc'
        head = mgetl(f, 2);
        if head(2) == header then
            fname = basename(f);
            break;
        end
    end
    
    v_in = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i)));
    v_out = variables(cpr.sim.outlnk(cpr.sim.outptr(blk_i)));
    z = "z" + string(blk_i);
        
    // Scilab variable definition (caller side)
    if n_outputs > 0 then
        outputs = strcat(v_out, ', ')
    else
        outputs = "";
    end
    if n_inputs > 0 then
        inputs = ", " + strcat(v_in, ', ');
    else
        inputs = ""
    end
    if n_state > 0 then
        dstate = ", z" + string(blk_i);
    else
        dstate = "";
    end
    
    // emit the call to the function
    gen_write_comment(blk_i, Info, fd);	
    select flag
    case 1 // output
        mputl("    [" + outputs + "] = " + fname + "(" + string(flag) + inputs + dstate +")", fd);
    case 2 // state
        mputl("    [" + outputs + dstate + "] = " + fname + "(" + string(flag) + inputs + dstate +")", fd);
    case 3 // event
    case 4 // initialization
        mputl("    [" + outputs + dstate + "] = " + fname + "(" + string(flag) + inputs + dstate +")", fd);
    case 5 // ending
        mputl("    " + fname + "(" + string(flag) + inputs + dstate +")", fd);
    case 6 // re-initialization
    case 7 // continuous properties
    case 9 // zero-crossing
    case 10 // jacobian
    end
endfunction
