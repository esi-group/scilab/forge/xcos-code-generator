// Copyright 2017 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_logicalop(blk_i, variables, Info, flag, fd)
    select flag
    case 1
        gen_write_comment(blk_i, Info, fd);

        n_port = cpr.sim.inpptr(blk_i+1) - cpr.sim.inpptr(blk_i);
        v_in = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i):cpr.sim.inpptr(blk_i)-1+n_port));
        v_out = variables(cpr.sim.outlnk(cpr.sim.outptr(blk_i)));

        // select the comparison
        rule = cpr.sim.ipar(cpr.sim.ipptr(blk_i))
        select rule 
        case 0 then op = strcat(v_in, " & ");
        case 1 then op = strcat(v_in, " | ");
        case 2 then op = "~( " + strcat(v_in, " & ") + " )";
        case 3 then op = "~( " + strcat(v_in, " | ") + " )";
        case 4 then op = "bitxor(" + strcat(v_in, ", ") + ")"; // only 2 inputs
        case 5 then op = "~" + v_in; // only 1 input
        end

        // cast the output to the right type
        xcs = Info(6);
        lnk = cpr.sim.outlnk(cpr.sim.outptr(blk_i));
        value = xcs.outtb(lnk);
        select typeof(value)
        case "constant" then
            cast = "double"
        else
            cast = typeof(value);
        end

        mputl("    " + v_out + " = " + cast + "(" + op + ");", fd);
    end
endfunction

