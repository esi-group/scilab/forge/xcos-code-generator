// Copyright 2016 - Scilab Enterprises - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_intrpl(blk_i, variables, Info, flag, fd)
    select flag
    case 1
    	gen_write_comment(blk_i, Info, fd);
    	
        v_out = variables(cpr.sim.outlnk(cpr.sim.outptr(blk_i)));
        v_in = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i)));
        
        n_points = (cpr.sim.rpptr(blk_i+1) - cpr.sim.rpptr(blk_i)) / 2;
        table = cpr.sim.rpar(cpr.sim.rpptr(blk_i):cpr.sim.rpptr(blk_i+1)-1);
        table = matrix(table, n_points, 2);
        table = string(table);
        
        // Algorithm from the reference intrpl.f file
        // y1=rpar(nrpar/2+i-1) + ((rpar(nrpar/2+i)-rpar(nrpar/2+i-1))/(rpar(i)-rpar(i-1)))*(u1-rpar(i-1))
        // transformed to
        // y1=rpar(i-1,2) + ((rpar(i,2)-rpar(i-1,2))/(rpar(i,1)-rpar(i-1,1)))*(u1-rpar(i-1,1))
        
        function str = interpolate(table, i, v_in)
            str = table(i-1, 2) + " + ((" + table(i, 2) +" - " + table(i-1, 2) + ") / (" + table(i, 1) + " - " + table(i-1, 1) + ")) * (" + v_in +" - " + table(i-1, 1) + ");";
        endfunction
        
        i = 2;
        mputl("    if " + v_in + " <= " + table(i, 1) + " then", fd)
        mputl("        " + v_out + " = " + interpolate(table, i, v_in), fd);
        for i=3:n_points-1
            mputl("    elseif " + v_in + " <= " + table(i, 1) + " then", fd)
            mputl("        " + v_out + " = " + interpolate(table, i, v_in), fd);
        end
        i = n_points;
        mputl("    else", fd)
        mputl("        " + v_out + " = " + interpolate(table, i, v_in), fd);
        mputl("    end", fd)
    end
endfunction
