// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function gen_matmul_m(blk_i, variables, Info, flag, fd)
    v_in1 = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i)));
    v_in2 = variables(cpr.sim.inplnk(cpr.sim.inpptr(blk_i) + 1));
    v_out = variables(cpr.sim.outlnk(cpr.sim.outptr(blk_i)));

    select flag
    case 1
        gen_write_comment(blk_i, Info, fd);

        rule = cpr.sim.ipar(cpr.sim.ipptr(blk_i));
        kmin = cpr.sim.rpar(cpr.sim.rpptr(blk_i));
        kmax = cpr.sim.rpar(cpr.sim.rpptr(blk_i)+1);

        select rule
        case 1 then
            op = " * "; // Matrix by Matrix
        case 2 then
            op = " .* "; // Matrix by Matrix element wise
        else
            op = " .* "; // Matrix by Scalar
        end

        mputl("    " + v_out + " = " + v_in1 + op + v_in2 + ";", fd);
    end
endfunction

