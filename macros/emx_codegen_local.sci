// Copyright 2016 - Scilab Enterprises - Clement DAVID
// Copyright 2017 - ESI Group - Clement DAVID
// 
// This software is governed by the CeCILL license under French law and
// abiding by the rules of distribution of free software.  You can  use, 
// modify and/ or redistribute the software under the terms of the CeCILL
// license as circulated by CEA, CNRS and INRIA at the following URL
// "http://www.cecill.info". 
// 
// As a counterpart to the access to the source code and  rights to copy,
// modify and redistribute granted by the license, users are provided only
// with a limited warranty  and the software's author,  the holder of the
// economic rights,  and the successive licensors  have only  limited
// liability. 
// 
// In this respect, the user's attention is drawn to the risks associated
// with loading,  using,  modifying and/or developing or reproducing the
// software by the user in light of its specific status of free software,
// that may mean  that it is complicated to manipulate,  and  that  also
// therefore means  that it is reserved for developers  and  experienced
// professionals having in-depth computer knowledge. Users are therefore
// encouraged to load and test the software's suitability as regards their
// requirements in conditions enabling the security of their systems and/or 
// data to be ensured and,  more generally, to use and operate it in the 
// same conditions as regards security. 
// 
// The fact that you are presently reading this means that you have had
// knowledge of the CeCILL license and that you accept its terms.
// 

function [rep,stat,err] = emx_codegen_local(in, args)
    // Helper function to call emmtrix CodeGenerator (eCG)
    
    [lhs, rhs] = argn();
    if 0 > rhs | rhs > 2 then
        error(sprintf(_("%s: Wrong number of input arguments: %d to %d expected.\n"), "emx_codegen_local", 0, 2));
    end

    if rhs > 0 & typeof(in) <> "string" then
        error(sprintf(_("%s: Argument #%d: File expected"), "emx_codegen_local", 1));
    end
    if rhs > 0 & fileinfo(in(:)) == [] then
        error(sprintf(_("%s: Argument #%d: Scilab script file expected"), "emx_codegen_local", 1));
    end
        
    if rhs > 1 & typeof(args) <> "st" then
        error(sprintf(_("%s: Argument #%d: Structure expected"), "emx_codegen_local", 2));
    end

    // look for the emx-codegen tool
    cmd = "emx-codegen ";
    [rep,stat,err]=unix_g(cmd + "--help")
    if stat <> 0 then
        cmd = fullfile(getenv("EMX_CODEGEN_PATH", ""), "emx-codegen") + " ";
        [rep,stat,err]=unix_g(cmd + "--help")
    end
    if stat <> 0 then
        error(sprintf(_("%s: Unable to detect emmtrix Code Generator ""emx-codegen"""), "emx_codegen_local"));
    end
    
    if rhs == 0 then
        cmd = cmd + "--help";
    end
    if rhs > 1 then
        // args is available
        for n=fieldnames(args)'
            cmd = cmd + "--" + n + " " + string(getfield(n, args)) + " ";
        end
    end
    if rhs > 0 then
        // in is available, pass only the first argument
        cmd = cmd + "--in " + in(1) + " ";
    end
    
    [rep,stat,err]=unix_g(cmd)
endfunction

