// This file is released under the 3-clause BSD license. See COPYING-BSD.

function res = urlencode(str)
    allowedchars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    allowedascii = zeros(256,1);
    allowedascii(ascii(allowedchars)) = 1;
    
    res = [];

    for c = ascii(str)
        if (allowedascii(c))
            res($+1) = c;
        else
            res = [res; ascii(sprintf('%%%02X', c))'];
        end
    end
    
    res = ascii(res);
endfunction
