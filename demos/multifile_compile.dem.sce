//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

chdir(get_absolute_file_path("multifile_compile.dem.sce"));

if ~isdir(fullfile(TMPDIR, 'build')) then
    createdir(fullfile(TMPDIR, 'build'))
end

disp('This demo demonstrate how to compile scilab scripts with multiple files')

emx_codegen_remote(['multifile.sce', 'matrixmul.sce'], fullfile(TMPDIR, 'build'));
scinotes(fullfile(TMPDIR, 'build', ['multifile.sce', 'matrixmul.sce', 'multifile.c', 'matrixmul.c']))
// emx_build(fullfile(TMPDIR, 'build'));
// emx_exec('matrixmul', fullfile(TMPDIR, 'build'));

