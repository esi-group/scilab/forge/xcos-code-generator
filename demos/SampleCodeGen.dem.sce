path = get_absolute_file_path("SampleCodeGen.dem.sce");

exec(fullfile(path, "SampleCodeGenInit.sce"));
xcos(fullfile(path, "SampleCodeGenModel.zcos"));

loadXcosLibs();
importXcosDiagram(fullfile(path, "SampleCodeGenModel.zcos"));

name = "SampleCodeGen";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
scinotes(fullfile(TMPDIR, name + ".sci"));

generated_scilab_files = [fullfile(TMPDIR, name+"_scenario.sce")
fullfile(TMPDIR, name + ".sci")
fullfile(TMPDIR, name + "_inputs.sci")
fullfile(TMPDIR, name + "_outputs.sci")];

emx_codegen(generated_scilab_files, struct("outdir", TMPDIR))
scinotes(TMPDIR + filesep() + name + ".c");

