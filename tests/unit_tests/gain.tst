[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/gain.zcos");

// generate some code for each superblock on TMPDIR
name = "scalar_gain";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "vector_gain";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "matrix_gain";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
