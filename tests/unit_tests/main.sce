
// simulation function of the CONVERT block
// each block is defined by a scilab function that managed parameters to view this one : edit CONVERT
function [y] = xcos_convert_sim(u, ipar)
    select ipar
        // special case : no conversion
    case 1 then
        y = u;

        // parameter : On overflow: nothing
    case 2 then
        y = int32(u);
    case 3 then
        y = int16(u);
    case 4 then
        y = int8(u);
    case 5 then
        y = uint32(u);
    case 6 then
        y = uint16(u);
    case 7 then
        y = uint8(u);
    case 8 then
        y = double(u);
    case 9 then
        y = double(u);
    case 10 then
        y = double(u);
    case 11 then
        y = double(u);
        // to be continuated for all parameters
        //     ....
    end
endfunction

// even simple simulation function are not inlined but should probably be
// a better interface prototype should be : function y = xcos_prod_sim(u) where u is list(u1,u2)
// this block has no parameters
function y = xcos_prod_sim(u1, u2)
    y = u1 .* u2;
endfunction

// main scheduler
function main()

    // buffer of each link
    // on Xcos, everything is typed and sized ; the simulator just pass pointers around
    data1 = int32(zeros(10,10));
    data2 = zeros(10,10);
    data3 = zeros(10,10);

    while(1) // without OS on tiny embedded boards , this kind of feature will be selected on configuration

        data1 = argo_input();
        // or data1(:,:) = argo_gensin(); if it's easier to proof sizes and types

        data2 = xcos_convert_sim(data1, [8]);
        // should be partially evaluated to : data2 = double(data1);

        data3 = xcos_prod_sim(data2, data2)

        argo_output(data3);
    end

end
