[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/sum.zcos");

 
name = "sum_vector";

// with name
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);
deletefile(TMPDIR + filesep() + name + ".sci");

// with index (using lookup)
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);
deletefile(TMPDIR + filesep() + name + ".sci");

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
