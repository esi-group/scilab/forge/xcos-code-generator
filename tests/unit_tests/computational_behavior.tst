// <-- ENGLISH IMPOSED -->

[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/behaviors.zcos");

// generate some code for the superblock on TMPDIR
name = "computational_behavior";
ok = xcg_codegenerator(scs_m, TMPDIR, name);

// execute and display the generated code
exec(TMPDIR + filesep() + name + ".sci", 1);

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
