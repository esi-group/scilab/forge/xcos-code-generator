[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/scifunc_states.zcos");

// generate some code for each superblock on TMPDIR
name = "scifunc_states";
deletefile(TMPDIR + filesep() + "scifunc_inner1.sci");

ok = xcg_codegenerator(scs_m, TMPDIR, name);

exec(TMPDIR + filesep() + name + ".sci", 1);
exec(TMPDIR + filesep() + "scifunc_inner1.sci", 1);

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
