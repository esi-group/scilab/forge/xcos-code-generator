// <-- NOT FIXED -->

[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/fromwsb.zcos");

// Create the inputs
V.time = (0.1:0.1:1)';
V.values = (1:10)';
V2.time = (0.1:0.1:1)';
V2.values = [(1:10)' (11:20)'];

// generate some code for each superblock on TMPDIR
name = "scalar_fromwsb";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "vector_fromwsb";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
