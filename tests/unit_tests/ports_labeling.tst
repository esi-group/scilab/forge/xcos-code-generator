name = "ports_labeling";
[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/unit_tests";

loadXcosLibs();
importXcosDiagram(path + "/ports_labeling.zcos");

scs_m_selected_path = [];
for i=1:length(scs_m.objs)
    if typeof(scs_m.objs(i)) == "Block" then
        if scs_m.objs(i).model.label == name then
            scs_m_selected_path = i;
            break;
        end
    end
end

// generate some code for the superblock on TMPDIR
ok = xcg_codegenerator(scs_m, TMPDIR, scs_m_selected_path);

// execute and display the generated code
exec(TMPDIR + filesep() + name + ".sci", 1);

emx_codegen(TMPDIR + filesep() + name + "_scenario.sce");
