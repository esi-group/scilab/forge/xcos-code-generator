// <-- NO CHECK REF -->

function [y1] = intrp2_ref(u1, u2)
    // ported from intrp2.f ; reference algorithm
    
    rpar = [1000;2000;3500;5500;9500;13000;99;100;200;220;310;350;30;30;30;30;30;30;30;30;30;30;30;30;1300;1300;1300;1300;1300;1300;1650;1650;1650;1650;1650;1650;1650;1650;1650;1650;2450;2450;1650;1650;1650;1650;2450;2450];
    ipar = [6;6];

    if u1 > rpar(ipar(1)) then
        i = ipar(1);
    else
        i = find(u1 <= rpar(2:ipar(1)), 1) + 1;
    end
    if u2 > rpar(ipar(1)+ipar(2)) then
        j = ipar(2);
    else
        j = find(u2 <= rpar(ipar(1) + (2:ipar(2))), 1) + 1;
    end

    vy1=rpar(ipar(1)+j-1)
    vy2=rpar(ipar(1)+j)
    vz1=rpar(ipar(1)+ipar(2)+(i-2)*ipar(2)+j-1)
    vz4=rpar(ipar(1)+ipar(2)+(i-2)*ipar(2)+j)
    vz2=rpar(ipar(1)+ipar(2)+(i-1)*ipar(2)+j-1)
    vz3=rpar(ipar(1)+ipar(2)+(i-1)*ipar(2)+j)
    vx1=rpar(i-1)
    vx2=rpar(i)
    y1=(1.0d0-(u2-vy1)/(vy2-vy1))*(vz1+(vz2-vz1)*(u1-vx1)/(vx2-vx1)) ..
    +((u2-vy1)/(vy2-vy1))*(vz4+(vz3-vz4)*(u1-vx1)/(vx2-vx1))

endfunction

function [y1] = intrp2_readable(u1, u2)
    // generated code
    
    intrp2_x = [1000;2000;3500;5500;9500;13000];
    intrp2_y = [99;100;200;220;310;350];
    intrp2_z = [30,30,1300,1650,1650,1650;30,30,1300,1650,1650,1650;30,30,1300,1650,1650,1650;30,30,1300,1650,1650,1650;30,30,1300,1650,2450,2450;30,30,1300,1650,2450,2450];
    
    //EMX?: emx_size(intrp2_i, 1, 1)
    if u1 > intrp2_x($) then
        intrp2_i = size(intrp2_x, 'r');
    else
        intrp2_i = find(u1 <= intrp2_x(2:$), 1) + 1;
    end
    //EMX?: emx_size(intrp2_j, 1, 1)
    if u2 > intrp2_y($) then
        intrp2_j = size(intrp2_y, 'r');
    else
        intrp2_j = find(u2 <= intrp2_y(2:$), 1) + 1;
    end
    
    intrp2_vx1 = intrp2_x(intrp2_i - 1);
    intrp2_vx2 = intrp2_x(intrp2_i);
    intrp2_vy1 = intrp2_y(intrp2_j - 1);
    intrp2_vy2 = intrp2_y(intrp2_j);
    intrp2_vz1 = intrp2_z(intrp2_j - 1, intrp2_i - 1);
    intrp2_vz4 = intrp2_z(intrp2_j, intrp2_i - 1);
    intrp2_vz2 = intrp2_z(intrp2_j - 1, intrp2_i);
    intrp2_vz3 = intrp2_z(intrp2_j, intrp2_i);
    y1 = (1 - (u2 - intrp2_vy1) ./ (intrp2_vy2 - intrp2_vy1)) .* (intrp2_vz1 + (intrp2_vz2 - intrp2_vz1) .* (u1 - intrp2_vx1) ./ (intrp2_vx2 - intrp2_vx1)) ..
    + ((u2 - intrp2_vy1) ./ (intrp2_vy2 - intrp2_vy1)) .* (intrp2_vz4 + (intrp2_vz3 - intrp2_vz4) .* (u1 - intrp2_vx1) ./ (intrp2_vx2 - intrp2_vx1));
    
endfunction

N=100
u1 = grand(1, N, "unf", 0, 15000)
u2 = grand(1, N, "unf", 0, 500)
y1_ref = zeros(1,N);
y1_readable = zeros(1,N);
for i=1:N
    y1_ref(i) = intrp2_ref(u1(i), u2(i));
    y1_readable(i) = intrp2_readable(u1(i), u2(i));
end

assert_checkequal(y1_ref, y1_readable);
