[libnames, libpath] = libraryinfo("xcos_code_generatorlib");
path = libpath + "/../tests/nonreg_tests";

loadXcosLibs();
importXcosDiagram(path + "/scheduling_with_const.zcos");

// generate some code for each superblock on TMPDIR
name = "inner_const";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "outer_const";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "outer3_const";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "outer4_const";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);

name = "outer5_const";
ok = xcg_codegenerator(scs_m, TMPDIR, name);
exec(TMPDIR + filesep() + name + ".sci", 1);
